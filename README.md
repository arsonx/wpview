# Wordpress pages viewer written in AngularJs #

This application was written for education needs. It`s goal is to show search form which can be used to find posts on specified blog (blog must by registered on wordpress.com, for example 'en.blog.wordpress.com') and then view it. 

This little app uses wordpress Rest API. Mainly this endpoint: https://developer.wordpress.com/docs/api/1.1/get/sites/%24site/posts/#apidoc-query

It is 'refresh page' resistant. Special service called ``solidState`` was written to achieve this.

For education needs, despide the it's a tiny app, the code is organized with so called 'domain style'.

### Demo ###

* http://wpview.arson.eu/

### Installation ###

* Clone this repository with git of course
* ``npm install``
* ``bower install``
* ``gulp``

### Run dev version ###

* Just run gulp command with no parameter
* ``gulp``

### Run production version ###

* ``gulp production``
* Configure your web server to point at ``/web`` directory

### Main libraries ###

* AngularJs
* Angular Material Design
* RequireJS

### Goods used ###

* Gulp
* npm
* Bower
* WebStorm IDE
* git
* linux

### TODO ###

There are some additional tasks that should be considered:

* update gulpfile to optimize js/css files in production mode
* write some unit tests with karma/jasmine