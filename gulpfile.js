'use strict';

var gulp = require('gulp');
var connect = require('gulp-connect');
var runSequence = require('run-sequence');
var del = require('del');
var arrayMap = require('array-map');

var settings = {
    directories: {
        bowerComponents: './bower_components',
        source: './source',
        dest: './web'
    },
    vendors: {
        js: {
            dest: '/js-vendors',
            files: [
                '/domReady/domReady.js',
                '/angular/angular.js',
                '/angular-animate/angular-animate.js',
                '/angular-aria/angular-aria.js',
                '/angular-loader/angular-loader.js',
                '/angular-material/angular-material.js',
                '/angular-messages/angular-messages.js',
                '/angular-route/angular-route.js',
                '/angular-ui-router/release/angular-ui-router.js',
                '/requirejs/require.js'
            ]
        },
        css: {
            dest: '/css-vendors',
            files: [
                '/angular-material/angular-material.css',
                '/angular-material/angular-material.layouts.css'
            ]
        }
    }
};

gulp.task('copy-source', function () {
    return gulp.src(settings.directories.source + '/**/*')
        .pipe(gulp.dest(settings.directories.dest));
});

gulp.task('copy-js-vendors', function () {
    var files;

    files = arrayMap(settings.vendors.js.files, function (vendor) {
        return settings.directories.bowerComponents + vendor;
    });

    return gulp.src(files)
        .pipe(gulp.dest(settings.directories.dest + settings.vendors.js.dest));

});

gulp.task('copy-css-vendors', function () {
    var files;

    files = arrayMap(settings.vendors.css.files, function (vendor) {
        return settings.directories.bowerComponents + vendor;
    });

    return gulp.src(files)
        .pipe(gulp.dest(settings.directories.dest + settings.vendors.css.dest));

});

gulp.task('clean', function () {
    return del([
        settings.directories.dest + '/*'
    ]);
});

gulp.task('runServer', function () {
    connect.server({
        root: 'web',
        livereload: false
    });
});

gulp.task('watch', function () {
    gulp.watch(settings.directories.source + '/**/*', {interval: 500}, ['copy-source']);
    return gulp;
});

gulp.task('production', function () {
    runSequence(
        'clean',
        'copy-source',
        'copy-js-vendors',
        'copy-css-vendors'
    );
});

gulp.task('default', function () {
    runSequence(
        'clean',
        'copy-source',
        'copy-js-vendors',
        'copy-css-vendors',
        'watch',
        'runServer'
    );
});
