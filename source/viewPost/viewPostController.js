'use strict';

define(['./viewPostModule'], function (viewPostModule) {
    return viewPostModule.controller('viewPostController', function ($scope, $state, $stateParams, solidStateService) {

        var post = $stateParams.post;

        // fetch post
        if (post) {
            // store it in localStorage
            solidStateService.set('viewPostController.$stateParams.post', post);
        } else {
            // try to load post from localStorage
            post = solidStateService.get('viewPostController.$stateParams.post');
        }

        // can't fetch post, give up...
        if (!post) {
            $state.go('search-form');
            return;
        }

        $scope.post = post;

        // scroll window top
        window.scrollTo(0, 0);

    });
});
