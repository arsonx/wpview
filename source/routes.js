'use strict';

define(['app', 'uiRouter'], function (app) {

    return app.config(function ($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise("/search-form");

        $stateProvider
            .state('search-form', {
                url: '/search-form',
                templateUrl: 'searchForm/searchForm.html',
                data: {
                    pageTitle: 'Search Form'
                }
            })

            .state('search-form.results', {
                url: '/results',
                params: {
                    results: null
                },
                templateUrl: 'searchResults/searchResults.html',
                data: {
                    pageTitle: 'Search Results'
                }
            })

            .state('view-post', {
                url: '/post',
                params: {
                    post: null
                },
                templateUrl: 'viewPost/viewPost.html',
                data: {
                    pageTitle: 'Single post view'
                }
            });

    });

});