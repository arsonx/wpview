'use strict';

define(['./searchFormModule'], function (searchFormModule) {
    return searchFormModule.controller('searchFormController', function ($scope, $state, $mdToast, wpApiService, solidStateService) {

        $scope.orderOptions = [
            {value: 'date', name: 'Date created'},
            {value: 'modified', name: 'Date update'},
            {value: 'title', name: 'Post title'}
        ];

        $scope.orderDirections = [
            {value: 'ASC', name: 'Ascending'},
            {value: 'DESC', name: 'Descending'}
        ];

        $scope.pending = false;

        // form default values
        var formDefaults = {
            site: '',
            phrase: '',
            orderBy: 'date',
            orderDir: 'DESC',
            postsLimit: ''
        };

        // fetch form values
        $scope.form = solidStateService.get('searchFormController.$scope.form') || angular.copy(formDefaults);

        this.submit = function (form) {
            if (form.$invalid) {
                return;
            }

            var params;
            params = {
                number: $scope.form.postsLimit,
                order_by: $scope.form.orderBy,
                search: $scope.form.phrase,
                order: $scope.form.orderDir
            };

            $scope.pending = true;
            wpApiService.getPosts($scope.form.site, params)
                .then(function (response) {
                    $state.go('search-form.results', {
                            results: response.data
                        })
                        .then(function () {
                            $scope.pending = false;
                        });
                }, function (response) {
                    $scope.pending = false;
                    $mdToast.showSimple('API says: ' + (response.data.message || 'Unknown error'));
                });

            // remember form values
            solidStateService.set('searchFormController.$scope.form', $scope.form);
        };

    });
});
