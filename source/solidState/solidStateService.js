'use strict';

define(['./solidStateModule'], function (solidStateModule) {
    return solidStateModule.service('solidStateService', function ($state, $stateParams) {
        var data = sessionStorage.getItem('solidState');

        data = JSON.parse(data) || {};

        var save = function () {
            sessionStorage.setItem('solidState', JSON.stringify(data));
        };

        this.set = function (key, value) {
            data[key] = value;
            save();
        };

        this.get = function (key) {
            return data[key];
        };

        this.delete = function (key) {
            delete data[key];
            save();
        };

    });
});
