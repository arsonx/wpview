'use strict';

define(['angular'], function (ng) {
    return ng.module('app.solidStateModule', []);
});
