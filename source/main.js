'use strict';

require.config({
    paths: {
        angular: 'js-vendors/angular',
        ngAria: 'js-vendors/angular-aria',
        ngAnimate: 'js-vendors/angular-animate',
        ngMaterial: 'js-vendors/angular-material',
        ngMessages: 'js-vendors/angular-messages',
        uiRouter: 'js-vendors/angular-ui-router',
        domReady: 'js-vendors/domReady'
    },
    waitSeconds: 0,
    shim: {
        angular: {
            exports: 'angular'
        },
        uiRouter: {
            deps: ['angular']
        },
        ngAnimate: {
            deps: ['angular']
        },
        ngAria: {
            deps: ['angular']
        },
        ngMaterial: {
            deps: ['angular', 'ngAria', 'ngAnimate']
        },
        ngMessages: {
            deps: ['angular']
        }
    },
    deps: [
        './bootstrap'
    ]
});