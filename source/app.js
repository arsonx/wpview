'use strict';

define([
    'angular',
    'ngMessages',
    'searchForm/index',
    'searchResults/index',
    'viewPost/index',
    'wpApi/index',
    'solidState/index'
], function (ng) {

    return ng.module('app', [
        'ngMaterial',
        'ngMessages',
        'ui.router',
        'app.searchFormModule',
        'app.searchResultsModule',
        'app.viewPostModule',
        'app.wpApiModule',
        'app.solidStateModule'
    ]);
});