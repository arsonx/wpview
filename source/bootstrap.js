'use strict';

define([
    'require',
    'angular',
    'app',
    'routes',
    'ngMaterial',
    'ngMessages'
], function (require, ng) {

    require(['domReady'], function (domReady) {
        domReady(function (doc) {
            ng.bootstrap(doc, ['app']);
        });
    });

});
