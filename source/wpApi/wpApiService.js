'use strict';
define(['./wpApiModule'], function (wpApiModule) {
    return wpApiModule.factory('wpApiService', function ($http) {
        var postsUrl;

        postsUrl = 'https://public-api.wordpress.com/rest/v1.1/sites/:site/posts/';

        return {
            getPosts: function (site, params) {
                var parsedUrl;

                params = params || {};
                parsedUrl = postsUrl.replace(':site', site);

                return $http.get(parsedUrl, {
                    params: params
                });
            }
        };

    });
});
