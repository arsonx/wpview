'use strict';

define(['angular'], function (ng) {
    return ng.module('app.searchResultsModule', [])
        .filter('unsafe', function ($sce) {
            return $sce.trustAsHtml;
        });
});
