'use strict';

define(['./searchResultsModule'], function (searchResultsModule) {
    return searchResultsModule.controller('searchResultsController', function ($scope, $state, $stateParams, wpApiService, solidStateService) {

        var results = $stateParams.results;

        // fetch results
        if (results) {
            // store it in localStorage
            solidStateService.set('searchResultsController.$stateParams.results', results);
        } else {
            // try to load results from localStorage
            results = solidStateService.get('searchResultsController.$stateParams.results');
        }

        // can't fetch results, give up...
        if (!results) {
            $state.go('search-form');
            return;
        }

        $scope.posts = results.posts;
        $scope.found = results.found;

        /**
         * View single post
         * @param post
         */
        this.viewPost = function (post) {
            $state.go('view-post', {post: post});
        };

    });
});
